#!/usr/bin/env bash

set -e
set -o pipefail

function print_header() {
    echo -e "\n***** ${1} *****"
}
print_header "RUN cppcheck"
cppcheck project --enable=all --error-exitcode=1 -I project/include --suppress=missingIncludeSystem

print_header "RUN cpplint.py"
cpplint --extensions=cpp --headers=h,hpp --filter=-runtime/references,-legal/copyright,-build/include_subdir,-whitespace/line_length project/include/* project/src/*

print_header "SUCCESS"
